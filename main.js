document.getElementById("hello_text").textContent="はじめてのJavaScript";
document.addEventListener("keydown",onKeyDown);

var count = 0;
var cells;

var block={
    i:{class:"i", pattern:[[1,1,2,1]]},
    o:{class:"o", pattern:[[1,1],[1,1]]},
    t:{class:"t", pattern:[[0,1,0],[1,2,1]]},
    s:{class:"s", pattern:[[0,1,1],[1,2,0]]},
    z:{class:"z", pattern:[[1,1,0],[0,2,1]]},
    j:{class:"j", pattern:[[1,0,0],[1,2,1]]},
    l:{class:"l", pattern:[[0,0,1],[1,2,1]]}
};

function onKeyDown(event){
    if(event.keyCode===37){
        moveLeft();
    }else if(event.keyCode===39){
        moveRight();
    }else if(event.keyCode===40){
        fallBlocks();
    }else if(event.keyCode===32){
        rotateRight();
    }
}

loadTable();
setInterval(function(){
    count++;
    document.getElementById("hello_text").textContent="はじめてのJavaScript("+count+")";

    for(var row=0; row<2; row++){
        for(var col=0; col<10; col++){
            if(cells[row][col].className!=="" && cells[row][col].blockNum!==fallingBlockNum){
                alert("GAME OVER");
            }
        }
    }

    if(hasFalledBlock()){
        fallBlocks();
    }else{
        deleteRow();
        generateBlock();
    }    
},1000);


function loadTable(){
    var td_array = document.getElementsByTagName("td");
    cells=[];
    var index=0;
    for (var row=0; row<20; row++){
        cells[row]=[];
        for (var col=0; col<10; col++){
            cells[row][col]=td_array[index];
            index++
        }
    }
}

function fallBlocks(){
    //落とす条件
    for(var i=0; i<10; i++){
        if(cells[19][i].blockNum===fallingBlockNum){
            isFalling=false;
            return;
        }
    }
    for(var row=18; row>=0; row--){
        for(var col=0; col<10; col++){
            if(cells[row][col].blockNum===fallingBlockNum){
                if(cells[row+1][col].className!=="" && cells[row+1][col].blockNum!==fallingBlockNum){
                    isFalling=false;
                    return;
                }
            }
        }
    }

    //下の段に落とす
    for(var row=18; row>=0; row--){
        for(var col=0; col<10; col++){
            if(cells[row][col].blockNum===fallingBlockNum){
                cells[row+1][col].className=cells[row][col].className;
                cells[row+1][col].blockNum=cells[row][col].blockNum;
                cells[row+1][col].axis=cells[row][col].axis;
                cells[row][col].className="";
                cells[row][col].blockNum=null;
                cells[row][col].axis=null;
            }
        }
    }
}

var isFalling=false;
function hasFalledBlock(){
    return isFalling;
}

function deleteRow(){
    var canDelete;
    for(var row=19; row>=0; row--){
        canDelete=true;
        for(var col=0; col<10; col++){
            if(cells[row][col].className===""){
                canDelete=false;
            }
        }
        if(canDelete){
            for(var i=row; i>0; i--){
                for(var j=0; j<10; j++){
                    cells[i][j].className=cells[i-1][j].className;
                    cells[i][j].blockNum=cells[i-1][j].blockNum;
                    cells[i][j].axis=cells[i-1][j].axis;
                }
            }
            for(var col=0; col<10; col++){
                cells[0][col].className="";
                cells[0][col].blockNum=null;
                cells[0][col].axis=null;
            }
            row++;
        }
    }
}

var fallingBlockNum=0;
function generateBlock(){
    var keys=Object.keys(block);
    var nextBlockKey=keys[Math.floor(Math.random()*keys.length)];
    var nextBlock=block[nextBlockKey];
    var nextFallingBlockNum=fallingBlockNum+1;

    var pattern=nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
          if (pattern[row][col]) {
            cells[row][col+3].className = nextBlock.class;
            cells[row][col+3].blockNum=nextFallingBlockNum;
          }
          if(pattern[row][col]==2){
              cells[row][col+3].axis=1;
          }
        }
    }

    isFalling=true;
    fallingBlockNum=nextFallingBlockNum;
}

function moveLeft(){
    for(var row=0; row<20; row++){
        if(cells[row][0].blockNum===fallingBlockNum){
            return;
        }
    }

    for(var row=0; row<20; row++){
        for(var col=1; col<10; col++){
            if(cells[row][col].blockNum===fallingBlockNum){
                if(cells[row][col-1].className!=="" && cells[row][col-1].blockNum!==fallingBlockNum){
                    return;
                }
            }
        }
    }

    for(var row=0; row<20; row++){
        for(var col=1; col<10; col++){
            if(cells[row][col].blockNum===fallingBlockNum){
                cells[row][col-1].className=cells[row][col].className;
                cells[row][col-1].blockNum=cells[row][col].blockNum;
                cells[row][col-1].axis=cells[row][col].axis;
                cells[row][col].className="";
                cells[row][col].blockNum=null;
                cells[row][col].axis=null;
            }
        }
    }
}

function moveRight(){
    for(var row=0; row<20; row++){
        if(cells[row][9].blockNum===fallingBlockNum){
            return;
        }
    }

    for(var row=0; row<20; row++){
        for(var col=8; col>=0; col--){
            if(cells[row][col].blockNum===fallingBlockNum){
                if(cells[row][col+1].className!=="" && cells[row][col+1].blockNum!==fallingBlockNum){
                    return;
                }
            }
        }
    }
    
    for(var row=0; row<20; row++){
        for(var col=8; col>=0; col--){
            if(cells[row][col].blockNum===fallingBlockNum){
                cells[row][col+1].className=cells[row][col].className;
                cells[row][col+1].blockNum=cells[row][col].blockNum;
                cells[row][col+1].axis=cells[row][col].axis;
                cells[row][col].className="";
                cells[row][col].blockNum=null;
                cells[row][col].axis=null;
            }
        }
    }
}

function rotateRight(){
    for(var row=19; row>=0; row--){
        for(var col=0; col<10; col++){
            if(cells[row][col].axis===1 && cells[row][col].blockNum===fallingBlockNum){
                
            }
        }
    }
}